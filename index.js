/*
BT1
*/
var soNguyenBt1 = 0;
var soDemBt1 = 0;
while (soDemBt1 < 10000) {
  soNguyenBt1++;
  soDemBt1 += soNguyenBt1;
}
document.getElementById("resultBt1").innerHTML = `Kết quả là: ${soNguyenBt1}`;

/*
BT2
*/
var mathBt2 = function () {
  let soN = document.getElementById("soN").value * 1;
  let bienDem = 1;
  let result = 0;
  while (bienDem < soN + 1) {
    let soX = document.getElementById("soX").value * 1;
    soX = soX * bienDem;
    console.log(soX);
    result += soX;
    bienDem++;
  }
  document.getElementById("resultBt2").innerHTML = result;
};

/*
BT3
*/
var mathBt3 = function () {
  let soNBt3 = document.getElementById("soNBt3").value * 1;
  let result = 1;
  for (var i = 1; i < soNBt3 + 1; i++) {
    result *= i;
  }
  document.getElementById("resultBt3").innerHTML = result;
};

/*
BT4
*/
var mathBt4 = function () {
  let result = '';
  for (var i = 1; i < 11; i++) {
    if (i % 2 != 0) {
      result += `<div class="bg-danger">Số lẻ ${i}</div>`;
    } else if (i % 2 == 0) {
      result += `<div class="bg-primary">Số chẵn ${i}</div>`;
    }
  }
  document.getElementById("resultBt4").innerHTML = result;
};
